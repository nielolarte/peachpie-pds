<?php

/**
 * Dummy classes that are usually expected by some plugins to exist.
 * This file is never included.
 * TODO: compiler should be able to ignore classes extending something that does not exist - report warning and then fail in run time.
 */

if (!class_exists("WP_CLI_Command"))
{
    /**
     * Base class for WP-CLI commands
     *
     * @package wp-cli
     */
    abstract class WP_CLI_Command { }
}

if (!class_exists("Debug_Bar_Panel"))
{
    /**
     * Extends Debug Bar plugin by adding a panel to show all Freemius API requests.
     *
     * @package freemius
     */
    abstract class Debug_Bar_Panel { }
}

if (!class_exists("WPML_Elementor_Module_With_Items"))
{
    /**
     * Class LAE_WPML_Carousel
     *
     * @package i18n
     */
    abstract class WPML_Elementor_Module_With_Items { }
}

if (!class_exists("WPML_Elementor_Module_With_Items"))
{
    /**
     * Class LAE_WPML_Carousel
     *
     * @package i18n
     */
    abstract class WPML_Elementor_Module_With_Items { }
}

if (!class_exists("OceanWP_ButterBean_Control"))
{
    /**
     * Buttonset control class.
     *
     * @package ButterBean
     */
    abstract class OceanWP_ButterBean_Control { }
}

if (!class_exists("PEAR_Error"))
{
    abstract class PEAR_Error { }
}