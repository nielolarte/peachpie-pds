<?php

namespace LivemeshAddons\Controls;

use Elementor\Base_Control;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A single Checkbox control
 *
 * @param string $default     Whether to initial it as checked. 'on' for checked, and '' (empty string) for unchecked
 *                            Default ''
 *
 * @since 1.0.0
 */
class LAE_Control_Checkbox extends Base_Control {

    public function get_type() {
        return 'lae-checkbox';
    }

    public function content_template() {
        ?>
        <label class="elementor-control-title">
            <input type="checkbox" data-setting="{{ data.name }}" />
            <span>{{{ data.label }}}</span>
        </label>
        <# if ( data.description ) { #>
            <div class="elementor-control-field-description">{{{ data.description }}}</div>
            <# } #>
        <?php
    }
}
